<?php
/**
 * Page view rates report page callback function
 * 
 * @todo get the user's name
 * @todo make $start_time configurable on the fly
 * @todo make the count(*) of the query configurable
 * @todo make the columns of the table sortable
 * @todo wrap the header titles in t()
 * 
 * @return string
 */
function page_view_rates_page() {
  drupal_add_css(drupal_get_path('module', 'page_view_rates') . '/page_view_rates.css');
  $output = '';
  if (!$_GET['start']) {
    $start_time = variable_get('cron_last', 0);
  } else {
    $start_time = $_GET['start'];
  }
  if (!$_GET['end']) {
    $end_time = time();
  } else {
    $end_time = $_GET['end'];
  }
  $elapsed = $end_time - $start_time;
  $filter_form = drupal_get_form('page_view_rates_filter');
  $i = 0;
  $result = db_query("SELECT a.uid as u, count(*) as totalpages, min(a.timestamp) as minT, max(a.timestamp) as maxT, count(*)/((max(a.timestamp) - min(a.timestamp))/60) as rate
    FROM {accesslog} a
    WHERE a.timestamp > '%d'
    AND a.timestamp < '%d'
    GROUP by a.uid
    HAVING count(*) > 5
    ORDER by rate DESC", $start_time, $end_time);
  $heading = t('This report is from the last cron run (@start) to now (@now). This was @minutes minutes ago.',
          array('@start' => format_date($start_time), '@now' => format_date($end_time), '@minutes' => $elapsed/60));
  $output = '<p>' . $heading . '</p>';
  //$output .= drupal_get_form('page_view_rates_filter');
  $num_rows = FALSE;
  while ($row = db_fetch_object($result)) {
    if ($i % 2) {
      $row_class = 'odd';
    } else {
      $row_class = 'even';
    }
    $user = user_load($row->u);
    $table_content .= '<tr class = "' . $row_class . '"><td>' . $row->u . '</td>';
    $table_content .= '<td>' . theme('username', $user) . '</td>';
    $table_content .= '<td>' . $row->totalpages . '</td>';
    $table_content .= '<td>' . format_date($row->maxT) . '</td>';
    $table_content .= '<td>' . format_date($row->minT) . '</td>';
    $table_content .= '<td>' . $row->rate . '</td></tr>';
    $i++;
    $num_rows = TRUE;
  }
  if ($num_rows) {
    $output .= $filter_form;
    $output .= '<table><tbody><thead><td>' . t('Drupal UID') .'</td><td>' . t('Username') . '</td><td>' . t('Total pages viewed') . '</td><td>' . t('Last page viewed') . '</td><td>' . t('First page view') . '</td><td>' . t('Pages/min') . '</td></thead>';
    $output .= $table_content;
    $output .= '</tbody></table>';
    return $output;
  }
  $output .= '<p><em>' . t('No accesses were made in the timespan above.') . '</em></p>';
  $output .= $filter_form;
  return $output;
}

/**
 * Form builder - Filter the results
 * 
 * @todo provide a conditional if the user has chosen to use the date_popup
 *       module
 * 
 * @param type $form_state
 * @return string 
 */
function page_view_rates_filter(&$form_state) {
  if (module_exists(date_popup)) {
    $widget_type = 'date_popup';
  } else {
    $widget_type = 'date_select';
  }
  
  if (!$_GET['start']) {
    $start_time = variable_get('cron_last', 0);
  } else {
    $start_time = $_GET['start'];
  }
  if (!$_GET['end']) {
    $end_time = time();
  } else {
    $end_time = $_GET['end'];
  }

  $date_format = 'Y-m-d H:i';
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => 'Adjust range of dates and minimum page views',
    '#collapsible' => TRUE,
    '#redirect' => url('admin/reports/page_view_rates'),
  );
  $form['filters']['start'] = array(
    '#type' => $widget_type,
    '#title' => t('Select start date'),
    '#default_value' => format_date($start_time, 'custom', $date_format),
    '#date_format' => $date_format,
    '#date_label_position' => 'within',
  );
  $form['filters']['end'] = array(
    '#type' => $widget_type,
    '#title' => t('Select end date'),
    '#default_value' => format_date($end_time, 'custom', $date_format),
    '#date_format' => $date_format,
    '#date_label_position' => 'within',
  );
  $form['filters']['submit'] = array(
    '#type' => 'submit',
    '#value'=> t('Filter'),
    '#submit' => array('page_view_rates_filter_submit'),
  );
  $form['filters']['#redirect'] = array(
    'admin/reports/page_view_rates?',
  );
  return $form;
}

function page_view_rates_filter_submit($form, &$form_state) {
  $start = strtotime($form_state['values']['start']);
  $end = strtotime($form_state['values']['end']);
  $form_state['redirect'] = array('admin/reports/page_view_rates', 'start=' . $start . '&end=' . $end);
}
